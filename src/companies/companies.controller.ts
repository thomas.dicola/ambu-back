import { Controller, Get, Param, ParseIntPipe, Query } from '@nestjs/common';
import { CompaniesService } from './companies.service';
import { Company } from '@prisma/client';
// import { GetCompaniesFilterDto } from './dto/get-companies-filter.dto';

@Controller('companies')
export class CompaniesController {
  constructor(private readonly companiesService: CompaniesService) {}

  @Get('all')
  async findAll(): Promise<Company[]> {
    return await this.companiesService.findAll();
  }

  @Get()
  async findWithQueries(
    @Query('name') name: string,
    @Query('location') location: string,
  ): Promise<Company[]> {
    return await this.companiesService.findWithQueries(name, location);
  }
}
