import { Injectable } from '@nestjs/common';
import { Company } from '@prisma/client';
import { PrismaService } from '../prisma.service';

@Injectable()
export class CompaniesService {
  constructor(private prisma: PrismaService) {}

  // Retourne la liste complète de company par ordre asc
  async findAll(): Promise<Company[]> {
    console.log('All Companies');
    try {
      // Requête SQL simple qui retourne la liste complète de société d'ambulances présente en base de donnée
      // SELECT * FROM company ORDER BY name ASC
      const companies = await this.prisma.Company.findMany({
        include: { location: true },
        orderBy: { name: 'asc' },
      });
      return companies;
    } catch (error) {
      throw new Error('Aucun résultats de la recherche');
    }
  }

  // Moteur de recherche
  async findWithQueries(name: string, location: string): Promise<Company[]> {
    console.log('name :', name);
    console.log('location : ', location);
    try {
      // Requête SQL avec jointure qui retourne la liste de société d'ambulances dont la ville est Lille
      // SELECT *
      // FROM ("ambudb"."Company")
      // LEFT JOIN ("ambudb"."Location")
      // ON ("ambudb"."Company"."locationId") = ("ambudb"."Location"."id")
      // WHERE ("ambudb"."Location"."city") LIKE ('%Lille%')
      const companies = await this.prisma.Company.findMany({
        where: {
          OR: [
            { name: { contains: name } },
            {
              location: {
                OR: [
                  { city: { contains: location } },
                  { postcode: { contains: location } },
                ],
              },
            },
          ],
        },
        include: { location: true },
      });

      return companies;
    } catch (error) {
      throw new Error('Aucun résultats de la recherche');
    }
  }
}
